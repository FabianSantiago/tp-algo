#include <time.h>
#include <stdio.h>

#include <QApplication>
#include <QDebug>

#include "tp3.h"
#include "HuffmanNode.h"

_TestMainWindow* w = nullptr;
using std::size_t;
using std::string;

void HuffmanHeap::insertHeapNode(int heapSize, unsigned char c, int frequences)
{
    int i = heapSize; 
    this->get(i).frequences = frequences;
    this->get(i).character = c;
    while(i>0 && this->get(i).frequences > this->get((i-1)/2).frequences)
    {
        swap(i, (i-1)/2);
        i=(i-1)/2;
    }
}

void HuffmanNode::insertNode(HuffmanNode* node)
{
    if(this->isLeaf() )
    {
        
        HuffmanNode* copy = new HuffmanNode(this->character, this->frequences);
        if (2*this->frequences > node->frequences)
            {
                this->left = node;
                this->right = copy;
            }
        else
            {
                this->left = copy;
                this->right = node;
            }
        this->character = '\0';
    }
    else
    {
        if (3*node->frequences < this->frequences)
            {
                this->left->insertNode(node);
            }
        else
            {
                this->right->insertNode(node);
            }

    }
    this->frequences += node->frequences;
}

void HuffmanNode::processCodes(std::string baseCode)
{
    
}

void HuffmanNode::fillCharactersArray(HuffmanNode** nodes_for_chars)
{
    
}

void charFrequences(string data, Array& frequences)
{

}

void huffmanHeap(Array& frequences, HuffmanHeap& heap, int& heapSize)
{
   
}

void huffmanDict(HuffmanHeap& heap, int heapSize, HuffmanNode*& dict)
{
   
}

string huffmanEncode(HuffmanNode** characters, string toEncode)
{

}

string huffmanDecode(HuffmanNode* dict, string toDecode)
{

}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Array::wait_for_operations = false;
    w = new _TestMainWindow();

    string data = "Ouesh, bien ou bien ? Ceci est une chaine de caracteres sans grand interet";

    Array& frequences = w->newArray(256);
    HuffmanHeap heap(256);
    HuffmanNode* dict;
    int i;

    for (i=0; i < (int)frequences.size(); ++i)
        frequences.__set__(i, 0);

    charFrequences(data, frequences);

    for (i=0; i < (int)frequences.size(); ++i)
        if (frequences[i]>0)
            qDebug() << (char)i << ": " << frequences[i];

    int heapSize=0;

    huffmanHeap(frequences, heap, heapSize);
    huffmanDict(heap, heapSize, dict);
    dict->processCodes("");

    HuffmanNode* characters[256];
    memset(characters, 0, 256 * sizeof (HuffmanNode*));
    dict->fillCharactersArray(characters);

    string encoded = huffmanEncode(characters, data);
    string decoded = huffmanDecode(dict, encoded);

    w->addBinaryNode(dict);
    w->updateScene();
    qDebug("Encoded: %s\n", encoded.c_str());
    qDebug("Decoded: %s\n", decoded.c_str());
    w->show();

    return a.exec();
}
